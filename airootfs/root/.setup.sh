#!/bin/zsh

ln -s /usr/share/applications/at-spi.desktop ~/.config/autostart/
ln -s /usr/share/applications/orca.desktop ~/.config/autostart/

source /home/seccom/.zsh-nvm/zsh-nvm.plugin.zsh
echo source /home/seccom/.zsh-nvm/zsh-nvm.plugin.zsh >> /home/seccom/.zshrc

cd /home/seccom/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/package-query.tar.gz
cd /home/seccom/; wget https://aur.archlinux.org/cgit/aur.git/snapshot/yaourt.tar.gz

tar xvf /home/seccom/package-query.tar.gz
tar xvf /home/seccom/yaourt.tar.gz

cd /home/seccom/package-query/; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/seccom/package-query/

cd /home/seccom/yaourt; makepkg; sudo pacman -U *.pkg.tar.xz --noconfirm
rm -r /home/seccom/yaourt/
rm /home/seccom/*.tar.gz

yaourt -S google-chrome --noconfirm
yaourt -S sublime-text --noconfirm
