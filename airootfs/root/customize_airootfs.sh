#!/bin/bash

set -e -u

sed -i 's/#\(pt_BR\.UTF-8\)/\1/' /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

sed -i "1s/^/Server = http:\/\/pet\.inf\.ufsc\.br\/mirrors\/archlinux\/\$repo\/os\/\$arch\n/" /etc/pacman.d/mirrorlist
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default graphical.target

rm etc/systemd/system/getty@tty1.service.d/autologin.conf

sed -i "s/#\(auto_login \).\+/\1yes/" /etc/slim.conf
sed -i "s/#\(default_user \).\+/\1seccom/" /etc/slim.conf

mv /root/at-spi.desktop /usr/share/applications/

systemctl enable slim
systemctl enable dhcpcd
systemctl enable NetworkManager

pacman-key --init
pacman-key --populate archlinux

cp /root/wallpaper_1360x728-01.png /usr/share/backgrounds/xfce/xfce-blue.jpg
mv /root/wallpaper_1360x728-01.png /usr/share/backgrounds/xfce/xfce-teal.jpg

useradd -m -s /usr/bin/zsh seccom
mv /root/.xinitrc /home/seccom/
touch /home/seccom/.zshrc
mv /root/.config /home/seccom/
mv /root/.setup.sh /home/seccom/
mv /root/.zsh-nvm /home/seccom/

chown -R seccom:seccom /home/seccom/

sed -i "s/# \(ALL ALL=(ALL) \).\+/\1NOPASSWD: ALL/" /etc/sudoers

su -c "/home/seccom/.setup.sh" -l seccom
rm /home/seccom/.setup.sh

sed -i "s/\(ALL ALL=(ALL).\+\)/\# \1/" /etc/sudoers
passwd -l root

